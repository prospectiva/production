#ifndef VARIABLE_H
#define VARIABLE_H

#include <QString>
#include <string>
#include "tools.h"
#include "src/objects/theme.h"

#include <QVariant>

class Variable
{
public:
    Variable();
    Variable(QString const& name, QString const& description, int theme=-1);
    QString name();
    QString description();
    int theme();
    void set(QString const& name="", QString const& description="", int theme=-1);
    void setName(QString const& name);
    void setDescription(QString const & description);
    void setTheme(int theme);

    //Serialize
    static void initVariableSystem ();

private:
    QString m_name;
    QString m_description;
    int m_theme;

    /*Serialize*/
    friend QDataStream & operator << (QDataStream &, const Variable &);
    friend QDataStream & operator >> (QDataStream &, Variable &);
};

Q_DECLARE_METATYPE(Variable)
QDataStream & operator << (QDataStream & out, const Variable & Valeur);
QDataStream & operator >> (QDataStream & in, Variable & Valeur);

#endif // VARIABLE_H
