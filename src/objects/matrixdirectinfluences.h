#ifndef MATRIXDIRECTINFLUENCES_H
#define MATRIXDIRECTINFLUENCES_H

#include "src/objects/variable.h"
#include "src/objects/variablelist.h"
#include <QString>
#include <QMap>
#include <QPair>
#include <QDataStream>
#include <QVariant>
#include <QDebug>

class MatrixDirectInfluences
{
public:
    MatrixDirectInfluences();
    MatrixDirectInfluences(VariableList* vars);
    enum Influence{Unknown, Yes, No};
    Influence get(QPair<Variable*,Variable*> pair);
    Influence get(Variable* var1, Variable* var2);
    void set(QPair<Variable*,Variable*> pair, Influence influence);
    void set(Variable* var1, Variable* var2, Influence influence);

    QMap<Variable*,QPair<int,int>>* directResults();
    QMap<Variable*,QPair<int,int>>* indirectResults(int const& deg);

    static QString influenceToString(Influence const& inf);
    static Influence stringToInfluence(QString const& inf);

    //Serialize
    static void initSystem ();
private:
    QMap<QPair<Variable*,Variable*>, Influence>* m_matrix;
    VariableList* m_variables;
    QVector<QVector<int>>* getMatrixIntVersion();

    /*Serialize*/
    friend QDataStream & operator << (QDataStream &, const MatrixDirectInfluences &);
    friend QDataStream & operator >> (QDataStream &, MatrixDirectInfluences &);
};

Q_DECLARE_METATYPE(MatrixDirectInfluences)
QDataStream & operator << (QDataStream & ds, const MatrixDirectInfluences & inObj);
QDataStream & operator >> (QDataStream & ds, MatrixDirectInfluences & inObj);

#endif // MATRIXDIRECTINFLUENCES_H
