#include "matrixdirectinfluences.h"

MatrixDirectInfluences::MatrixDirectInfluences(){}
MatrixDirectInfluences::MatrixDirectInfluences(VariableList* vars): m_variables(vars) {
    m_matrix = new QMap<QPair<Variable*,Variable*>, Influence>;
}

MatrixDirectInfluences::Influence MatrixDirectInfluences::get(QPair<Variable *, Variable *> pair){
    if(pair.first==pair.second){ return Influence::No; }
    if(m_matrix->size()==0) {return Influence::Unknown;}

    return m_matrix->value(pair,Influence::Unknown);
}
MatrixDirectInfluences::Influence MatrixDirectInfluences::get(Variable *var1, Variable *var2){
    QPair<Variable*,Variable*> pair = QPair<Variable*,Variable*>(var1,var2);
    return get(pair);
}

void MatrixDirectInfluences::set(QPair<Variable*,Variable*> pair, Influence influence){
    m_matrix->insert(pair,influence);
}
void MatrixDirectInfluences::set(Variable *var1, Variable *var2, Influence influence){
    QPair<Variable*,Variable*> pair = QPair<Variable*,Variable*>(var1,var2);
    set(pair,influence);
}

//TODO use it everywhere
QString MatrixDirectInfluences::influenceToString(Influence const& inf){
    if(inf==MatrixDirectInfluences::Influence::Yes)
        return "1";
    if(inf==MatrixDirectInfluences::Influence::No)
        return "0";
    return "";
}
MatrixDirectInfluences::Influence MatrixDirectInfluences::stringToInfluence(QString const& inf){
    if(inf=="1")
        return MatrixDirectInfluences::Influence::Yes;
    if(inf=="0")
        return MatrixDirectInfluences::Influence::No;
    return MatrixDirectInfluences::Influence::Unknown;
}

QMap<Variable*,QPair<int,int>>* MatrixDirectInfluences::directResults(){
    QMap<Variable*,QPair<int,int>>* result = new QMap<Variable*,QPair<int,int>>();

    for(int i(0); i<<m_variables->size(); ++i)
    {
        result->insert(m_variables->get(i),QPair<int,int>(0,0));
    }

    for(QMap<QPair<Variable*,Variable*>, MatrixDirectInfluences::Influence>::iterator
        i=m_matrix->begin();i!=m_matrix->end();++i){
        if(i.value()==MatrixDirectInfluences::Influence::Yes){
            (*result)[i.key().first].first+=1;
            (*result)[i.key().second].second+=1;
        }
    }
    return result;
}

QMap<Variable*,QPair<int,int>>* MatrixDirectInfluences::indirectResults(int const &deg){
    QVector<QVector<int>>* matrix = getMatrixIntVersion(); // stay at matrix
    QVector<QVector<int>>* tempValue = new QVector<QVector<int>>();  // to be matrix^i for i<deg
    QVector<QVector<int>>* result = new QVector<QVector<int>>(); // to the calcul save part
    QVector<Variable*> variableList = m_variables->list();

    // set tempValue to I_n and initialize result
    for(int i(0);i<matrix->size();++i)
    {
        result->push_back(QVector<int>());
        tempValue->push_back(QVector<int>());
        for(int j(0);j<matrix->size();++j){
            int value = i==j?1:0;
            (*tempValue)[i].push_back(value);
            (*result)[i].push_back(0);
        }
    }

    //Calcul of matrix^deg = result
    for(int d=1; d<=deg; d++){
        //set result to O_n
        for(int i(0);i<matrix->size();++i)
        {
            for(int j(0);j<matrix->size();++j){
                (*result)[i][j]=0;
            }
        }

        //calcul of multiplication matrix : tempValue*matrix = result
        try
            { //we want to stop if a value is <0 (ie. >MaxInt)
                for(int i = 0; i < matrix->size(); ++i)
                        for(int j = 0; j < matrix->size(); ++j)
                            for(int k = 0; k < matrix->size(); ++k)
                            {
                                (*result)[i][j] += (*matrix)[i][k] * (*tempValue)[k][j];
                                if((*result)[i][j]<0)
                                    throw 1;
                            }
                // here, result = tempValue*matrix, so result = matrix^i
                *tempValue = QVector<QVector<int>>(*result);
        }
        catch(int e){
            if(e==1) {// value>MaxInt
                //set result to 0
                for(int i(0);i<result->size();++i)
                    for(int j = 0; j < matrix->size(); ++j)
                        (*result)[i][j]=0;
            }
        }
    }

    //write in output format
    QMap<Variable*,QPair<int,int>>* returnValue = new QMap<Variable*,QPair<int,int>>();
    for(int i(0); i<<m_variables->size(); ++i)
    {
        returnValue->insert(m_variables->get(i),QPair<int,int>(0,0));
    }
    int index(0);
    for(QVector<Variable*>::iterator i=variableList.begin();i!=variableList.end();++i){
            //calcul inf and dep
            int inf(0);
            int dep(0);
            for(int k(0);k<matrix->size();++k){
                inf+=(*result)[index][k]; // sum of rows
                dep+=(*result)[k][index]; // sum of cols
            }
            //set value
            (*returnValue)[*i].first+=inf;
            (*returnValue)[*i].second+=dep;
       ++index;
    }

    return returnValue;
}

QVector<QVector<int>>* MatrixDirectInfluences::getMatrixIntVersion(){
    //get matrix only with 1 and 0
    QVector<QVector<int>>* matrix = new QVector<QVector<int>>();

    QVector<Variable*> variableList = m_variables->list();
    int value(0);

    for(QVector<Variable*>::iterator i=variableList.begin(); i!=variableList.end(); ++i)
    {
        matrix->push_back(QVector<int>());
        for(QVector<Variable*>::iterator j=variableList.begin(); j!=variableList.end(); ++j)
        {
            value= this->get(*i,*j)==Influence::Yes?1:0;
            (*matrix)[matrix->size()-1].push_back(value);
        }
    }

    return matrix;
}

/*Serializing Influence*/
QDataStream& operator<<(QDataStream &ds, const MatrixDirectInfluences::Influence &inObj)
{
    QString value("");
    if(inObj==MatrixDirectInfluences::Influence::Yes) {value="1";}
    else if(inObj==MatrixDirectInfluences::Influence::No) {value="0";}
    else {value="";}

    ds << value;
    return ds;
}
QDataStream & operator >> (QDataStream & ds, MatrixDirectInfluences::Influence &inObj)
{
    QString value;
    ds >> value;
    if(value=="1") {inObj = MatrixDirectInfluences::Influence::Yes;}
    else if(value=="0") {inObj = MatrixDirectInfluences::Influence::No;}
    else {inObj = MatrixDirectInfluences::Influence::Unknown;}

    return ds;
}

//Serialize
void MatrixDirectInfluences::initSystem ()
{
    qRegisterMetaTypeStreamOperators<MatrixDirectInfluences>("MatrixDirectInfluences");

    qMetaTypeId<MatrixDirectInfluences>();//test if valide
}

QDataStream & operator << (QDataStream & ds, const MatrixDirectInfluences & inObj)
{
    size_t from;
    size_t to;

    ds << static_cast<quint64>(inObj.m_matrix->size());

    for(QMap<QPair<Variable*,Variable*>, MatrixDirectInfluences::Influence>::iterator
        it=inObj.m_matrix->begin();it!=inObj.m_matrix->end();++it){
          from = inObj.m_variables->index(it.key().first);
          to = inObj.m_variables->index(it.key().second);
        ds << QString::number(from) << QString::number(to) << it.value();
    }

    return ds;
}
QDataStream & operator >> (QDataStream & ds, MatrixDirectInfluences & inObj)
{
    quint64 tempSize;
    ds >> tempSize;
    QString from,to;
    QPair<Variable*,Variable*> pair;
    MatrixDirectInfluences::Influence value;

    for(quint64 i=0;i<tempSize;++i){
        ds >> from;
        ds >> to;
        ds >> value;

        pair.first = inObj.m_variables->get(from.toInt());
        pair.second = inObj.m_variables->get(to.toInt());

        inObj.set(pair,value);
    }

    return ds;
}
