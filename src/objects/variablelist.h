#ifndef VARIABLELIST_H
#define VARIABLELIST_H

#include <QString>
#include <QDataStream>
#include <vector>
#include <QDebug>
#include "variable.h"

#include <QVariant>
#include <QVector>
#include <ostream>

class VariableList
{
public:
    VariableList();
    void push(Variable *variable);
    Variable* get(size_t const& i);
    Variable* get(int const& i);
    size_t index(Variable* var);
    std::vector<Variable*> exportVector();
    QVector<Variable*> list() const;
    QStringList namesInQStringList();
    int size();
    void remove(int const& i);

    //Serialize
    static void initVariableListSystem ();

private:
    QVector<Variable*> m_list;

    /*Serialize*/
    friend QDataStream & operator << (QDataStream &, const VariableList &);
    friend QDataStream & operator >> (QDataStream &, VariableList &);
};

Q_DECLARE_METATYPE(VariableList)
QDataStream & operator << (QDataStream & out, const VariableList & Valeur);
QDataStream & operator >> (QDataStream & ds, VariableList & inObj);

#endif // VARIABLELIST_H
