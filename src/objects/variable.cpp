#include "variable.h"

Variable::Variable(){}
Variable::Variable(QString const& name,QString const& description, int theme):
    m_name(name), m_description(description), m_theme(theme)
{
}

QString Variable::name(){
    return m_name;
}
QString Variable::description(){
    return m_description;
}
int Variable::theme(){
    return m_theme;
}
void Variable::set(const QString &n, const QString &d, int theme){
    if(n!="")
        m_name=n;
    if(d!="")
        m_description=d;
    if(theme!=-1)
        m_theme=theme;
}
void Variable::setName(const QString &name){
    if(name!="")
        m_name=name;
}
void Variable::setDescription(const QString &description){
    m_description=description;
}
void Variable::setTheme(int theme){
    m_theme=theme;
}

//Serialize
void Variable::initVariableSystem ()
{
    qRegisterMetaTypeStreamOperators<Variable>("Variable");

    qMetaTypeId<Variable>();//test if valide
}

QDataStream & operator << (QDataStream & out, const Variable & Valeur)
{
    out << Valeur.m_name
        << Valeur.m_description
        << QString::number(Valeur.m_theme);

    return out;
}
QDataStream & operator >> (QDataStream & in, Variable & Valeur)
{
    in >> Valeur.m_name;
    in >> Valeur.m_description;
    QString a;
    in >> a;
    Valeur.m_theme = a.toInt();

    return in;
}
