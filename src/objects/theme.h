#ifndef THEME_H
#define THEME_H

#include <QString>
#include <string>
#include "tools.h"

#include <QVariant>

class Theme
{
public:
    Theme();
    Theme(QString const& name, QString const& description="");
    QString name();
    QString description();

    //Serialize
    static void initThemeSystem ();

private:
    QString m_name;
    QString m_description;

    /*Serialize*/
    friend QDataStream & operator << (QDataStream &, const Theme &);
    friend QDataStream & operator >> (QDataStream &, Theme &);
};

Q_DECLARE_METATYPE(Theme)
QDataStream & operator << (QDataStream & out, const Theme & Valeur);
QDataStream & operator >> (QDataStream & in, Theme & Valeur);

#endif // THEME_H
