#include "variablelist.h"

VariableList::VariableList()
{
}

void VariableList::push(Variable *variable){
    m_list.push_back(variable);
}
Variable* VariableList::get(const size_t& i) {
        return get(static_cast<int>(i));
}
Variable* VariableList::get(const int& i) {
    if(i<m_list.size())
        return m_list[i];
    else
        qDebug() << "try to access to VariableList[" << i << "]";
    return nullptr;
}
size_t VariableList::index(Variable *var){
    return m_list.indexOf(var);
}
std::vector<Variable*> VariableList::exportVector(){
    std::vector<Variable*> vec;
    for(QVector<Variable*>::iterator it=m_list.begin(); it!=m_list.end(); ++it)
    {
        vec.push_back(*it);
    }
    return vec;
}

QVector<Variable*> VariableList::list() const{
    return m_list;
}
int VariableList::size(){
    return m_list.size();
}
void VariableList::remove(const int &i){
    m_list.remove(i);
}
QStringList VariableList::namesInQStringList(){
    QStringList l;
    for (QVector<Variable*>::iterator it=m_list.begin();it!=m_list.end();++it) {
        QString name = (*it)->name();
        if(!l.contains(name))
            l.push_back(name);
    }
    return l;
}

//Serialize
void VariableList::initVariableListSystem ()
{
    qRegisterMetaTypeStreamOperators<VariableList>("VariableList");

    qMetaTypeId<VariableList>();//test if valide
}

//TODO: update with << and >> Variable*, defined below ?
QDataStream& operator<<(QDataStream &ds, const VariableList &inObj)
{
    ds << static_cast<quint64>(inObj.list().size());
    for(QVector<Variable*>::iterator it=inObj.list().begin(); it!=inObj.list().end(); ++it)
        ds << **it;
    return ds;
}

QDataStream & operator >> (QDataStream & ds, VariableList & inObj)
{
    quint64 tempSize;
    ds >> tempSize;
    Variable* var;

    inObj.m_list.resize(static_cast<int>(tempSize));
    for(quint64 i=0;i<tempSize;++i){
        var = new Variable();
        ds >> *var;
        inObj.m_list[static_cast<int>(i)] = var;
    }

    return ds;
}

//For Variable*
/*
QDataStream & operator<< (QDataStream & ds, Variable* inObj){
    ds << *inObj;
    return ds;
}
QDataStream & operator >> (QDataStream & ds, Variable* inObj){
    Variable* var = new Variable();
    ds >> *var;
    inObj = var;
    return ds;
}
*/
