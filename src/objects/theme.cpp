#include "theme.h"

Theme::Theme(){}
Theme::Theme(QString const& name,QString const& description): m_name(name), m_description(description)
{
}

QString Theme::name(){
    return m_name;
}
QString Theme::description(){
    return m_description;
}


//Serialize
void Theme::initThemeSystem ()
{
    qRegisterMetaTypeStreamOperators<Theme>("Theme");

    qMetaTypeId<Theme>();//test if valide
}

QDataStream & operator << (QDataStream & out, const Theme & Valeur)
{
    out << Valeur.m_name
        << Valeur.m_description;

    return out;
}
QDataStream & operator >> (QDataStream & in, Theme & Valeur)
{
    in >> Valeur.m_name;
    in >> Valeur.m_description;

    return in;
}
