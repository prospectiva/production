#include "calcedit.h"

void CalcEdit::write(const QString &filename, QList<QList<QString>> *table){
    QXlsx::Document xlsxW;
    QXlsx::CellReference ref = QXlsx::CellReference("A1");
    QList<QString>* row = new QList<QString>();

    for (int i(0);i<table->size();++i) {
        *row = table->at(i);
        for (int j(0);j<row->size();++j) {
            xlsxW.write(ref,row->at(j));
            ref.setColumn(ref.column()+1);
        }
        ref.setRow(ref.row()+1);
        ref.setColumn(1);
    }
    xlsxW.autosizeColumnWidth();
    xlsxW.saveAs(filename);
}

QList<QList<QString>>* CalcEdit::read(const QString& filename, QString const& firstCell, int numberRows, int numberCols){
    QList<QList<QString>>* table = new QList<QList<QString>>();
    QXlsx::Document xlsxR(filename);
    QList<QString>* rowValue = new QList<QString>();
    QXlsx::CellReference ref = QXlsx::CellReference(firstCell);

    int firstRow(ref.row()), firstCol(ref.column());
    // firstCell have to match with (0,0) in the return table

    QXlsx::Cell* cell = new QXlsx::Cell();

    if (xlsxR.load()) // load excel file
    {
        //0 in params = read all table
        int number_of_rows = numberRows>0?numberRows:xlsxR.dimension().lastRow();
        int number_of_cols = numberCols>0?numberCols:xlsxR.dimension().lastColumn();

        for (int row = firstRow; row <= number_of_rows; ++row) {
            rowValue->clear();
            ref.setRow(row);
            for(int col = firstCol; col <= number_of_cols; ++col){
                ref.setColumn(col);
                QString value("");
                cell = xlsxR.cellAt(ref);
                value = cell?cell->value().toString():"";
                rowValue->push_back(value);
            }
            table->push_back(*rowValue);
        }
    }
    return table;
}
