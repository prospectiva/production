#ifndef GRAPHGROUPING_H
#define GRAPHGROUPING_H

#include "src/objects/variablelist.h"
#include "src/objects/variable.h"

class GroupingElement;
class GraphGrouping
{
public:
    GraphGrouping();
    GraphGrouping(QString const& name, QList<GroupingElement>* elements=new QList<GroupingElement>());
    QString name();
private:
    QString m_name;
    QList<GroupingElement>* m_elements;

};

class GroupingElement {
public:
    enum Type{Var,Grouping};
    GroupingElement();
    GroupingElement(Variable* var);
    GroupingElement(GraphGrouping* grouping);

private:
    Type m_type;
    Variable* m_var;
    GraphGrouping* m_grouping;
};

#endif // GRAPHGROUPING_H
