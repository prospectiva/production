#include "themelist.h"

ThemeList::ThemeList()
{
}

void ThemeList::push(Theme *theme){
    m_list.push_back(*theme);
}
Theme ThemeList::get(const int& i) {
    return m_list[i];
}
std::vector<Theme> ThemeList::exportVector(){
    std::vector<Theme> vec;
    for(QVector<Theme>::iterator it=m_list.begin(); it!=m_list.end(); ++it)
    {
        vec.push_back(*it);
    }
    return vec;
}

QVector<Theme> ThemeList::list() const{
    return m_list;
}
QStringList ThemeList::namesInQStringList(){
    QStringList l;
    for (QVector<Theme>::iterator it=m_list.begin();it!=m_list.end();++it) {
        if(!l.contains(it->name()))
            l.push_back(it->name());
    }
    return l;
}
int ThemeList::size(){
    return m_list.size();
}
int ThemeList::find(const QString &themeName){
    int a(0);
    for (QVector<Theme>::iterator it=m_list.begin();it!=m_list.end();++it) {
        if(it->name()==themeName)
            return a;
        ++a;
    }
    return -1; //if not found
}

//Serialize
void ThemeList::initThemeListSystem ()
{
    qRegisterMetaTypeStreamOperators<ThemeList>("ThemeList");

    qMetaTypeId<ThemeList>();//test if valide
}

QDataStream& operator<<(QDataStream &ds, const ThemeList &inObj)
{
    ds << static_cast<quint64>(inObj.list().size());
    for(QVector<Theme>::iterator it=inObj.list().begin(); it!=inObj.list().end(); ++it)
        ds << *it;
    return ds;
}

QDataStream & operator >> (QDataStream & ds, ThemeList & inObj)
{
    quint64 tempSize;
    ds >> tempSize;

    inObj.m_list.resize(static_cast<int>(tempSize));
    for(quint64 i=0;i<tempSize;++i)
        ds >> inObj.m_list[static_cast<int>(i)];
    return ds;
}
