#include "project.h"
using namespace std;

Project::Project():
    Project(QString("Projet"),QString(""))
{}

Project::Project(QString const& name, QString const& path):m_projectName(name), m_dir(path)
{
    m_projectId = baseFile();
    m_variables = new VariableList();
    m_themes = new ThemeList();
    m_anaStruct = new AnaStruct(m_variables);
}

QString Project::name() {
    return m_projectName;
}
QString Project::directory() {
    return m_dir;
}
QString Project::id(){
    return m_projectId;
}
VariableList* Project::variables(){
    return m_variables;
}
ThemeList* Project::themes(){
    return m_themes;
}
AnaStruct* Project::anaStruct(){
    return m_anaStruct;
}
QString Project::baseFile() {//return name in file mode
    string str=Tools::qStringToString(this->name());
    //Upper after spaces
    for(string::iterator i=str.begin();i!=str.end();++i) {
        if(*i==' ' and i!=str.end()-1){
            *(i+1) = toupper(*(i+1));
        }
    }
    //lower first letter
    str[0]=tolower(str[0]);
    //delete spaces
    str.erase(std::remove(str.begin(), str.end(), ' '),str.end());
    return Tools::stringToQString(str);
}

QString Project::file(){ //return file place
    return directory()+"/"+baseFile()+".proj";
}

bool Project::save(){//TODO check if return false in case of error
    QSettings out(file(),QSettings::NativeFormat);
    //Project infos
    Project::initProjectSystem();
    out.setValue("Project", qVariantFromValue(*this));

    //write
    out.sync();
    return true;
}

void Project::close(){
    int reponse = QMessageBox::question(nullptr,"Fermeture","Enregistrer le projet (" + this->name() + ") avant fermeture ?", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
    if (reponse == QMessageBox::Yes)
    {
        this->save();
    }
}

void Project::setVariables(VariableList* vars) {
    m_variables = vars;
}
void Project::setThemes(ThemeList* themes) {
    m_themes = themes;
}
void Project::setAnaStruct(AnaStruct *anaStruct) {
    m_anaStruct = anaStruct;
}

//Serialize
void Project::initProjectSystem ()
{
    qRegisterMetaTypeStreamOperators<Project>("Project");

    qMetaTypeId<Project>();//test if valide
}

QDataStream & operator << (QDataStream & out, const Project & Valeur)
{
    out << Valeur.m_projectId
        << Valeur.m_dir
        << Valeur.m_projectName
        << * Valeur.m_variables
        << * Valeur.m_themes
        << * Valeur.m_anaStruct
           ;

    return out;
}
QDataStream & operator >> (QDataStream & in, Project & Valeur)
{
    in >> Valeur.m_projectId;
    in >> Valeur.m_dir;
    in >> Valeur.m_projectName;

    VariableList* vL = new VariableList();
    in >> *vL;
    Valeur.m_variables = vL;

    ThemeList* tL = new ThemeList();
    in >> *tL;
    Valeur.m_themes = tL;

    AnaStruct* aS = new AnaStruct(Valeur.m_variables);
    in >> *aS;
    Valeur.m_anaStruct = aS;

    return in;
}
