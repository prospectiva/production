#ifndef TOOLS_H
#define TOOLS_H

#include <string>
#include <QString>

class Tools
{
public:
    Tools();
    static std::string qStringToString(QString const& str);
    static QString stringToQString(std::string const& str);
};

#endif // TOOLS_H
