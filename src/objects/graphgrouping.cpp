#include "graphgrouping.h"

/* Graph goruping */
GraphGrouping::GraphGrouping()
{
    m_elements=nullptr;
}
GraphGrouping::GraphGrouping(const QString &name, QList<GroupingElement>* elements): m_name(name), m_elements(elements) {
}
QString GraphGrouping::name(){
    return m_name;
}

/* Grouping Element */
GroupingElement::GroupingElement(): m_var(nullptr),m_grouping(nullptr) {}
GroupingElement::GroupingElement(Variable* var): m_type(Type::Var),m_var(var),m_grouping(nullptr){}
GroupingElement::GroupingElement(GraphGrouping* grouping): m_type(Type::Grouping),m_var(nullptr),m_grouping(grouping){}
