#ifndef ANASTRUCT_H
#define ANASTRUCT_H

#include <QDataStream>
#include <QVariant>
#include "src/objects/matrixdirectinfluences.h"
#include "src/objects/variablelist.h"
#include "src/objects/graphgrouping.h"
#include <QtDebug>

class AnaStruct
{
public:
    AnaStruct();
    AnaStruct(VariableList* vars);
    MatrixDirectInfluences* getMatrixDI();
    QList<GraphGrouping*>* groupings();
    void addGrouping(QString const& name);

    //Serialize
    static void initSystem ();
private:
    MatrixDirectInfluences* m_matrix;
    QList<GraphGrouping*>* m_groupings;
    VariableList* m_variables;

    /*Serialize*/
    friend QDataStream & operator << (QDataStream &, const AnaStruct &);
    friend QDataStream & operator >> (QDataStream &, AnaStruct &);
};

Q_DECLARE_METATYPE(AnaStruct)
QDataStream & operator << (QDataStream & ds, const AnaStruct & inObj);
QDataStream & operator >> (QDataStream & ds, AnaStruct & inObj);

#endif // ANASTRUCT_H
