#include "anastruct.h"

AnaStruct::AnaStruct(){}
AnaStruct::AnaStruct(VariableList* vars): m_variables(vars){
    m_matrix = new MatrixDirectInfluences(vars);
    m_groupings = new QList<GraphGrouping*>;
}
MatrixDirectInfluences* AnaStruct::getMatrixDI(){
    return m_matrix;
}
QList<GraphGrouping*>* AnaStruct::groupings(){
    return m_groupings;
}
void AnaStruct::addGrouping(const QString &name){
    //check if name exist
    bool nameExist=false;
    for(QList<GraphGrouping*>::iterator it=m_groupings->begin(); it!=m_groupings->end();++it){
        if((*it)->name()==name){
            nameExist=true;break;}
    }

    if(!nameExist){
        GraphGrouping* group = new GraphGrouping(name);
        m_groupings->push_back(group);
    }
}

//Serialize
void AnaStruct::initSystem ()
{
    qRegisterMetaTypeStreamOperators<AnaStruct>("AnaStruct");

    qMetaTypeId<AnaStruct>();//test if valide
}

QDataStream & operator << (QDataStream & ds, const AnaStruct & inObj)
{
    ds << *(inObj.m_matrix);
    return ds;
}
QDataStream & operator >> (QDataStream & ds, AnaStruct & inObj)
{
    MatrixDirectInfluences* m = new MatrixDirectInfluences(inObj.m_variables);
    ds >> *m;
    inObj.m_matrix = m;
    return ds;
}
