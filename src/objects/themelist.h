#ifndef THEMELIST_H
#define THEMELIST_H


#include <QString>
#include <QDataStream>
#include <vector>
#include "theme.h"

#include <QVariant>
#include <QVector>
#include <ostream>

class ThemeList
{
public:
    ThemeList();
    void push(Theme *theme);
    Theme get(int const& i);
    std::vector<Theme> exportVector();
    QVector<Theme> list() const;
    QStringList namesInQStringList();
    int find(QString const& themeName);
    int size();

    //Serialize
    static void initThemeListSystem ();

private:
    QVector<Theme> m_list;

    /*Serialize*/
    friend QDataStream & operator << (QDataStream &, const ThemeList &);
    friend QDataStream & operator >> (QDataStream &, ThemeList &);
};

Q_DECLARE_METATYPE(ThemeList)
QDataStream & operator << (QDataStream & out, const ThemeList & Valeur);
QDataStream & operator >> (QDataStream & ds, ThemeList & inObj);

#endif // THEMELIST_H
