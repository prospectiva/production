#ifndef CALCEDIT_H
#define CALCEDIT_H

#include <QString>
#include <QTableWidget>

/*QXlsx*/
#include "xlsxdocument.h"
#include "xlsxchartsheet.h"
#include "xlsxcellrange.h"
#include "xlsxcellreference.h"
#include "xlsxchart.h"
#include "xlsxrichstring.h"
#include "xlsxworkbook.h"

class CalcEdit
{
public:
    CalcEdit();
    static void write(QString const& filename, QList<QList<QString>>* table);
    static QList<QList<QString>>* read(QString const& filename, QString const& firstCell="A1", int numberRows=0,int numberCols=0);
};

#endif // CALCEDIT_H
