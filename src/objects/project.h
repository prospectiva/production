#ifndef PROJECT_H
#define PROJECT_H

#include <iostream>
#include <string>
#include <QString>
#include <fstream>
#include <sstream>
#include <ostream>
#include <algorithm>
#include "tools.h"
#include "variablelist.h"
#include "themelist.h"
#include "src/objects/anastruct.h"
#include <QMessageBox>

#include <QVariant>
#include <QSettings>

class AnaStruct;

class Project
{
public:
    Project();
    Project(QString const& name, QString const& directory);
    QString name();
    QString directory();
    QString id();
    VariableList* variables();
    ThemeList* themes();
    QString file();
    AnaStruct* anaStruct();
    void correctPtr();

    bool save();
    void close();
    void setVariables(VariableList* vars);
    void setThemes(ThemeList* themes);
    void setAnaStruct(AnaStruct* anaStruct);

    //Serialize
    static void initProjectSystem ();

private:
    QString baseFile();
    QString m_projectName;
    QString m_projectId;
    QString m_dir;
    VariableList *m_variables;
    ThemeList *m_themes;
    AnaStruct *m_anaStruct;

    /*Serialize*/
    friend QDataStream & operator << (QDataStream &, const Project &);
    friend QDataStream & operator >> (QDataStream &, Project &);
};

Q_DECLARE_METATYPE(Project)
QDataStream & operator << (QDataStream & out, const Project & Valeur);
QDataStream & operator >> (QDataStream & in, Project & Valeur);

#endif // PROJECT_H
