#include "openedprojectarea.h"
#include "ui_openedprojectarea.h"

OpenedProjectArea::OpenedProjectArea(Project* project, QWidget *parent) :
    QMdiArea(parent),
    ui(new Ui::OpenedProjectArea)
{
    ui->setupUi(this);
    m_project = project;
    OpenedProjectInfos *infos = new OpenedProjectInfos(m_project);
    addSubWindow(infos);
}

OpenedProjectArea::~OpenedProjectArea()
{
    delete ui;
}

void OpenedProjectArea::displayVariables(){
    VariableDisplayWindow *window = new VariableDisplayWindow(m_project);
    QMdiSubWindow *subwindow = addSubWindow(window);
    subwindow->setGeometry(window->geometry());
    subwindow->show();
}
void OpenedProjectArea::displayMatrixDI(){
    AnaStructDefinitionInfluences *window = new AnaStructDefinitionInfluences(m_project);
    QMdiSubWindow *subwindow = addSubWindow(window);
    subwindow->setGeometry(window->geometry());
    subwindow->show();
}
void OpenedProjectArea::displayAnaStructResults(){
    AnaStructResults *window = new AnaStructResults(m_project);
    QMdiSubWindow *subwindow = addSubWindow(window);
    subwindow->setGeometry(window->geometry());
    subwindow->show();
}
