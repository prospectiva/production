#include "variabledisplaywindow.h"
#include "ui_variabledisplaywindow.h"

VariableDisplayWindow::VariableDisplayWindow(Project* project, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VariableDisplayWindow),m_project(project)
{
    ui->setupUi(this);
    displayModel();
    updateModelTreeThemeView();
}

VariableDisplayWindow::~VariableDisplayWindow()
{
    delete ui;
}

void VariableDisplayWindow::displayModel(){
    QVector<Variable*> variableList = m_project->variables()->list();
    QStringList themeList = m_project->themes()->namesInQStringList();
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(3);
    int r(0);
    ui->idComboBox->clear();

    QList<QString> labels = QList<QString>();
    labels.push_back("Nom");
    labels.push_back("Description");
    labels.push_back("Thème");
    ui->tableWidget->setHorizontalHeaderLabels(labels);

    for(QVector<Variable*>::iterator i=variableList.begin(); i!=variableList.end(); ++i)
    {
        //creation of table line
        ui->tableWidget->insertRow(r);
        QTableWidgetItem* name = new QTableWidgetItem((*i)->name());
        QTableWidgetItem* descr = new QTableWidgetItem((*i)->description());
        int themeIndex = (*i)->theme();
        QTableWidgetItem* themeName = new QTableWidgetItem(
                    themeIndex==-1?"":themeList[themeIndex]
                );

        ui->tableWidget->setItem(r,0,name);
        ui->tableWidget->setItem(r,1,descr);
        ui->tableWidget->setItem(r,2,themeName);

        //for selecting id
        ui->idComboBox->addItem(QString::number(r+1));

        ++r;
    }

    //for selecting theme
    ui->thMeComboBox->clear();
    ui->thMeComboBox->addItem("");
    ui->thMeComboBox->addItems(themeList);

    if(m_project->variables()->size()>0)
        select(0);

    connect(ui->tableWidget,SIGNAL(itemChanged(QTableWidgetItem *)),this,SLOT(tableWidget_itemChanged(QTableWidgetItem *)));
}

void VariableDisplayWindow::updateModelTreeThemeView(){
    m_modeleTreeThemeView = new QStandardItemModel();
    QStringList themeList = m_project->themes()->namesInQStringList();
    QStandardItem *item;
    bool haveVarsWithoutTheme = false;
    for(QStringList::iterator it=themeList.begin();it!=themeList.end();++it){
         item = new QStandardItem(*it);
         m_modeleTreeThemeView->appendRow(item);
    }

    QVector<Variable*> variableList = m_project->variables()->list();
    for(QVector<Variable*>::iterator it=variableList.begin(); it!=variableList.end(); ++it)
    {
        int i = (*it)->theme();
        item = new QStandardItem((*it)->name());
        if(i>-1){
            m_modeleTreeThemeView->item(i)->appendRow(item);
        } else {
            if(!haveVarsWithoutTheme){
                //Add one for variablew without themes
                QStandardItem *row = new QStandardItem("Pas de thème");
                m_modeleTreeThemeView->appendRow(row);
                haveVarsWithoutTheme=true;
            }
            m_modeleTreeThemeView->item(m_modeleTreeThemeView->rowCount()-1)->appendRow(item);
        }
    }

    ui->treeThemesView->setModel(m_modeleTreeThemeView);
}


void VariableDisplayWindow::refresh(){
    displayModel();
}
void VariableDisplayWindow::select(int a){
    Variable* v=m_project->variables()->get(a);
    ui->idComboBox->setCurrentText(QString::number(a+1)); //to begin at index 1 in table, instead of 0
    ui->nomLineEdit->setText(v->name());
    ui->descriptionLineEdit->setText(v->description());
    if(v->theme()>-1)
        ui->thMeComboBox->setCurrentText(m_project->themes()->get(v->theme()).name());
    else
        ui->thMeComboBox->setCurrentText("");
}

/********* EXPORT AND IMPORT */
void VariableDisplayWindow::dealExport(const QString &file){
    QList<QList<QString>>* table = new QList<QList<QString>>();
    QVector<Variable*> list = m_project->variables()->list();
    QList<QString>* row;

    for (QVector<Variable*>::iterator it=list.begin();it!=list.end();++it) {
        row = new QList<QString>;
        // name
        row->push_back((*it)->name());
        //description
        row->push_back((*it)->description());
        //theme
        row->push_back(
                    (*it)->theme()==-1?"":m_project->themes()->get((*it)->theme()).name()
                );
        //add row
        table->push_back(*row);
    }
    CalcEdit::write(file,table);
}


int VariableDisplayWindow::dealImport(const QString &file){
    QList<QList<QString>>* table = CalcEdit::read(file,"A1",0,3);

    for(QList<QList<QString>>::iterator it=table->begin();it<table->end();++it){
        QString name(""),description(""),nameOfTheme("");
        if((*it)[0]!=""){
            name = (*it)[0];
            description = (*it)[1];
            nameOfTheme = (*it)[2];

            int indexOfTheme = -1;
            if(nameOfTheme!=""){ //set indexOfTheme to right value
                indexOfTheme = m_project->themes()->find(nameOfTheme);
                if(indexOfTheme==-1) {//if not exist
                    m_project->themes()->push(new Theme(nameOfTheme));
                    indexOfTheme = m_project->themes()->size()-1;
                }
            }

            Variable* var = new Variable(name,description,indexOfTheme);
            m_project->variables()->push(var);
        }
    }
    refresh();
        return 0;
}

/********************* end EXPORT AND IMPORT */

void VariableDisplayWindow::dealDelete(const QString &id){
    int i = id.toInt();
    m_project->variables()->remove(i-1); //-1 to begin with 1 instead of 0
    refresh();
}

void VariableDisplayWindow::on_refresh_clicked()
{
    refresh();
}

void VariableDisplayWindow::on_add_clicked()
{
    VariableNewWindow *window = new VariableNewWindow(m_project->variables(), this);
    window->exec();
    refresh();
}

void VariableDisplayWindow::on_idComboBox_activated(const QString &arg1)
{
    int a = arg1.toInt()-1; // -1 to begin id with 1 instead 0
    select(a);
}

void VariableDisplayWindow::on_update_clicked()
{
    size_t i = static_cast<size_t>(ui->idComboBox->currentText().toInt())-1;// -1 to begin id with 1 instead 0
    Variable* v = m_project->variables()->get(i);

    //get index of theme
    //TODO change this if with a function (see tableWidget_changeValue)
    int themeIndex(-1);
    if(ui->thMeComboBox->currentText()!=""){
        themeIndex=m_project->themes()->find(ui->thMeComboBox->currentText());
        if(themeIndex==-1) {//if not finded: creation of the theme
            Theme* theme = new Theme(ui->thMeComboBox->currentText());
            m_project->themes()->push(theme);
            themeIndex = m_project->themes()->size()-1;
        }
    }

    v->set(ui->nomLineEdit->text(),ui->descriptionLineEdit->text(),themeIndex);

    //correction if we want to delete the theme
    if(ui->thMeComboBox->currentText()=="") {
        v->setTheme(-1);
    }
    refresh();
}

void VariableDisplayWindow::on_tableWidget_activated(const QModelIndex &index)
{
    select(index.row());
}

void VariableDisplayWindow::on_tableWidget_doubleClicked(const QModelIndex &index)
{
    select(index.row());
}

void VariableDisplayWindow::on_tableWidget_pressed(const QModelIndex &index)
{
    select(index.row());
}

void VariableDisplayWindow::on_export_2_clicked()
{
    QString file = QFileDialog::getSaveFileName(this, "Exporter la liste des variables en csv", QString());
    dealExport(file);
}

void VariableDisplayWindow::on_delete_2_clicked()
{
    QString str = "Confirmez-vous la suppression de la variable " + ui->nomLineEdit->text() +" ?";
    if(QMessageBox::Yes==QMessageBox::question(this,"Confirmation",str)){
        dealDelete(ui->idComboBox->currentText());
    }
}

void VariableDisplayWindow::on_import_2_clicked()
{
    QString file = QFileDialog::getOpenFileName(this, "Importer des variables", QString());
    dealImport(file);
}

void VariableDisplayWindow::tableWidget_itemChanged(QTableWidgetItem *item){
    Variable* var = m_project->variables()->get(item->row());
    if(item->column()==0) {//name
           var->setName(item->text());
    }
    if(item->column()==1) {//description
           var->setDescription(item->text());
    }
    if(item->column()==2) {//theme
        QString themeName = item->text();
        int themeIndex(-1);

        //TODO change this if with a function
        if(themeName!=""){
            themeIndex=m_project->themes()->find(themeName);
            if(themeIndex==-1) {//if not finded: creation of the theme
                Theme* theme = new Theme(themeName);
                m_project->themes()->push(theme);
                themeIndex = m_project->themes()->size()-1;
            }
        }
        var->setTheme(themeIndex);
    }
    select(item->row());
    updateModelTreeThemeView();
}
