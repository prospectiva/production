#ifndef OPENEDPROJECTAREA_H
#define OPENEDPROJECTAREA_H

#include <QMdiArea>
#include <QMdiSubWindow>
#include <QMessageBox>
#include "src/objects/project.h"
#include "src/windows/openedprojectinfos.h"
#include "src/windows/variabledisplaywindow.h"
#include "src/windows/anastructdefinitioninfluences.h"
#include "src/windows/anastructresults.h"

namespace Ui {
class OpenedProjectArea;
}

class OpenedProjectArea : public QMdiArea
{
    Q_OBJECT

public:
    explicit OpenedProjectArea(Project* project, QWidget *parent = nullptr);
    ~OpenedProjectArea();
    void displayVariables();
    void displayMatrixDI();
    void displayAnaStructResults();

private:
    Ui::OpenedProjectArea *ui;
    Project *m_project;
};

#endif // OPENEDPROJECTAREA_H
