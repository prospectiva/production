#include "createproject.h"
#include "ui_createproject.h"

CreateProject::CreateProject(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateProject),
    main(parent)
{
    ui->setupUi(this);
    connect(this,SIGNAL(projectCreated(QString)),main,SLOT(projectCreation(QString)));
}

CreateProject::~CreateProject()
{
    delete ui;
}

void CreateProject::on_parcourirButton_clicked(){
    QFileDialog fileDialogWindow(this, "Créer un projet", QString());
    fileDialogWindow.setFileMode(QFileDialog::Directory);
    connect(&fileDialogWindow, SIGNAL(fileSelected(QString)), this->ui->dossierLineEdit, SLOT(setText(QString)));
    fileDialogWindow.exec();
}
void CreateProject::on_buttonBox_accepted(){
    QString name = this->ui->nomLineEdit->text();
    QString dir = this->ui->dossierLineEdit->text();
    if(name=="" || dir=="") {
     QMessageBox::warning(this,"Attention","Un des champs n'a pas été rempli.");
    }
    else{
        Project *project = new Project(name,dir);
        try {
            bool ret = project->save();
            if(ret){
                emit projectCreated(project->file());
                QMessageBox::information(this,"Nouveau projet","Le projet " + name + " a bien été créé.");
            }
        } catch (std::string const& str) {
            QMessageBox::critical(this,"Erreur",QString::fromStdString(str));
        }
    }
}

void CreateProject::on_buttonBox_rejected(){}
