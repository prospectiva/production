#ifndef VARIABLENEWWINDOW_H
#define VARIABLENEWWINDOW_H

#include <QDialog>
#include <src/objects/variable.h>
#include <src/objects/variablelist.h>

namespace Ui {
class VariableNewWindow;
}

class VariableNewWindow : public QDialog
{
    Q_OBJECT

public:
    explicit VariableNewWindow(VariableList* const variableListPt, QWidget *parent = nullptr);
    ~VariableNewWindow();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::VariableNewWindow *ui;
    VariableList* m_variableListPt;
};

#endif // VARIABLENEWWINDOW_H
