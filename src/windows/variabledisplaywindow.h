#ifndef VARIABLEDISPLAYWINDOW_H
#define VARIABLEDISPLAYWINDOW_H

#include <QWidget>
#include <QStandardItemModel>
#include <QStandardItem>
#include <vector>
#include "src/objects/variable.h"
#include "src/objects/variablelist.h"
#include "src/objects/project.h"
#include "src/windows/variablenewwindow.h"
#include <QLineEdit>
#include <QFileDialog>
#include <QString>
#include <fstream>
#include <ostream>
#include "src/objects/calcedit.h"

#include <QDebug>

#include <QMessageBox> //to delete

namespace Ui {
class VariableDisplayWindow;
}

class VariableDisplayWindow : public QWidget
{
    Q_OBJECT

public:
    explicit VariableDisplayWindow(Project* const project, QWidget *parent = nullptr);
    ~VariableDisplayWindow();

private slots:
    void on_refresh_clicked();

    void on_add_clicked();

    void on_idComboBox_activated(const QString &arg1);

    void on_update_clicked();

    void on_tableWidget_activated(const QModelIndex &index);

    void on_tableWidget_doubleClicked(const QModelIndex &index);

    void on_tableWidget_pressed(const QModelIndex &index);

    void on_export_2_clicked();

    void on_delete_2_clicked();

    void on_import_2_clicked();

    void tableWidget_itemChanged(QTableWidgetItem *);

private:
    void refresh();
    void select(int a);
    Ui::VariableDisplayWindow *ui;
    QStandardItemModel *m_modele;
    QStandardItemModel *m_modeleTreeThemeView;
    Project* m_project;
    void displayModel();
    void updateModelTreeThemeView();
    void dealExport(QString const & file);
    void dealDelete(QString const& id);
    int dealImport(QString const& file);
};

#endif // VARIABLEDISPLAYWINDOW_H
