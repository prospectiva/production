#ifndef ANASTRUCTDEFINITIONINFLUENCES_H
#define ANASTRUCTDEFINITIONINFLUENCES_H

#include <QWidget>
#include <QStandardItem>
#include <QStandardItemModel>
#include "src/objects/anastruct.h"
#include "src/objects/project.h"
#include <QTableWidgetItem>
#include <QMessageBox>
#include <QDebug>
#include <QFileDialog>
#include <regex>
#include "src/objects/calcedit.h"
#include "src/objects/tools.h"

namespace Ui {
class AnaStructDefinitionInfluences;
}

class AnaStructDefinitionInfluences : public QWidget
{
    Q_OBJECT

public:
    explicit AnaStructDefinitionInfluences(Project* project, QWidget *parent = nullptr);
    ~AnaStructDefinitionInfluences();

private slots:
    void tableWidget_itemChanged(QTableWidgetItem *item);

    void on_importButton_clicked();

    void on_exportButton_clicked();

    void on_influencesReciproques_stateChanged(int arg1);

    void on_dispAdvParams_clicked();

    void on_cycle_clicked();

private:
    Ui::AnaStructDefinitionInfluences *ui;
    Project* m_project;
//    QStandardItemModel *m_model;
    void displayModel();
    void displayModelAt(int const& row, int const& col, MatrixDirectInfluences::Influence const& influence=MatrixDirectInfluences::Influence::Unknown);
    void changeValue(int const& row, int const& col, MatrixDirectInfluences::Influence const& influence);
    void dealExport(QString const & file);
    void dealImport(QString const& file);
};

#endif // ANASTRUCTDEFINITIONINFLUENCES_H
