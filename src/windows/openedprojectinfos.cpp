#include "openedprojectinfos.h"
#include "ui_openedprojectinfos.h"

OpenedProjectInfos::OpenedProjectInfos(Project* project, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OpenedProjectInfos),
    m_project(project)
{
    ui->setupUi(this);
    ui->projectNameLabel->setText(m_project->name());
    QString size = QString::number(m_project->variables()->size());
    ui->variablesValue->setText(size);
}

OpenedProjectInfos::~OpenedProjectInfos()
{
    delete ui;
}
