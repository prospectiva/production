#include "variablenewwindow.h"
#include "ui_variablenewwindow.h"

VariableNewWindow::VariableNewWindow(VariableList* const varPt, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VariableNewWindow),
    m_variableListPt(varPt)
{
    ui->setupUi(this);
}

VariableNewWindow::~VariableNewWindow()
{
    delete ui;
}

void VariableNewWindow::on_buttonBox_accepted()
{
    Variable* var = new Variable(ui->nomLineEdit->text(),ui->descriptionLineEdit->text());
    m_variableListPt->push(var);
}
