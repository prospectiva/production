#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), m_mainProject(nullptr)
{
    ui->setupUi(this);
    setWindowTitle("ProspectiVA");
    QMainWindow::showMaximized();
    ui->menuVariables->setEnabled(false);
    ui->menuAnalyse->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionNouveauProjet_triggered(){
    CreateProject *window = new CreateProject(this);
    window->exec();
}

void MainWindow::on_actionOuvrirProjet_triggered(){
    QString filename = QFileDialog::getOpenFileName(this, "Ouvrir un projet", QString(), "ProspectiVA projects (*.proj)");
    if(filename!=QString(""))
        openProject(filename);
}
void MainWindow::openProject(QString const& filename) {

    if(m_mainProject!=nullptr){
        m_mainProject->close();
        m_mainProject=nullptr;
    }

    Project::initProjectSystem();
    VariableList::initVariableListSystem();
    ThemeList::initThemeListSystem();
    AnaStruct::initSystem();

    //Load project data
    QSettings in(filename,QSettings::NativeFormat);
    Project p = in.value("Project", qVariantFromValue(Project())).value<Project>();
    m_mainProject = new Project(p);


    //change title
    QString addTitle = m_mainProject->name();
    setWindowTitle(windowTitle().append(QString(" | ")).append(addTitle));
    //Set center area
    OpenedProjectArea *page = new OpenedProjectArea(m_mainProject);
    m_projectArea=page;
    setCentralWidget(page);
    //enable project actions
    ui->actionSauvegarder->setEnabled(true);
    ui->menuVariables->setEnabled(true);
    ui->menuAnalyse->setEnabled(true);
}

void MainWindow::on_actionAjouter_une_variable_triggered()
{
    VariableNewWindow *window = new VariableNewWindow(m_mainProject->variables(), this);
    window->exec();
}

Project* MainWindow::currentProject(){
    return m_mainProject;
}

void MainWindow::on_actionVoir_les_variables_triggered()
{
    m_projectArea->displayVariables();
}

void MainWindow::on_actionSauvegarder_triggered()
{
    m_mainProject->save();
    QMessageBox::information(this,"Sauvegarde","Le projet a bien été sauvegardé");
}

void MainWindow::projectCreation(QString const& file){
    openProject(file);
}
void MainWindow::closeEvent(QCloseEvent *event){
    event->ignore();
    m_mainProject->close();
    event->accept();
}

void MainWindow::on_actionD_finition_des_influences_triggered()
{
    m_projectArea->displayMatrixDI();
}

void MainWindow::on_actionR_sultats_triggered()
{
    m_projectArea->displayAnaStructResults();
}

void MainWindow::on_actionDocumentation_triggered() {
    //TODO: set urls in variables
    QDesktopServices::openUrl (QUrl("https://prospectiva.gitlab.io/"));
}
void MainWindow::on_actionLicence_triggered() {
    License *window = new License(this);
    window->exec();
}
