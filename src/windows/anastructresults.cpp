#include "anastructresults.h"
#include "ui_anastructresults.h"

AnaStructResults::AnaStructResults(Project* project, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AnaStructResults),m_project(project)
{
    ui->setupUi(this);
    displayDI();

    m_defaultDegII=6;
    displayII(m_defaultDegII);
    ui->degValue->setValue(m_defaultDegII);
}

AnaStructResults::~AnaStructResults()
{
    delete ui;
}

//TODO fusion of displayDI and displayII : a lot of lines are equal

void AnaStructResults::displayDI(){
    QMap<Variable*,QPair<int,int>>* result = m_project->anaStruct()->getMatrixDI()->directResults();
    QVector<Variable*> variableList = m_project->variables()->list();
    int N = variableList.size();
    ui->directTable->setRowCount(N);
    ui->directTable->setColumnCount(4);
    int r(0);

    QList<QString> labels = QList<QString>();
    labels.push_back("N°");
    labels.push_back("Nom");
    labels.push_back("Influence");
    labels.push_back("Dépendance");
    ui->directTable->setHorizontalHeaderLabels(labels);

    for(QVector<Variable*>::iterator var=variableList.begin(); var!=variableList.end(); ++var)
    {

        QTableWidgetItem* num = new QTableWidgetItem();
        QTableWidgetItem* name = new QTableWidgetItem((*var)->name());
        QTableWidgetItem* infItem = new QTableWidgetItem();
        QTableWidgetItem* depItem = new QTableWidgetItem();

        num->setData(Qt::EditRole,r+1);
        infItem->setData(Qt::EditRole,(*result)[*var].first);
        depItem->setData(Qt::EditRole,(*result)[*var].second);

        ui->directTable->setItem(r,0, num);
        ui->directTable->setItem(r,1, name);
        ui->directTable->setItem(r,2, infItem);
        ui->directTable->setItem(r,3, depItem);
        ++r;
    }
}

void AnaStructResults::displayII(int const& deg){
    QMap<Variable*,QPair<int,int>>* result = m_project->anaStruct()->getMatrixDI()->indirectResults(deg);
    QVector<Variable*> variableList = m_project->variables()->list();
    int N = variableList.size();
    ui->indirectTable->setRowCount(N);
    ui->indirectTable->setColumnCount(4);
    int r(0);

    QList<QString> labels = QList<QString>();
    labels.push_back("N°");
    labels.push_back("Nom");
    labels.push_back("Influence");
    labels.push_back("Dépendance");
    ui->indirectTable->setHorizontalHeaderLabels(labels);

    for(QVector<Variable*>::iterator var=variableList.begin(); var!=variableList.end(); ++var)
    {
        QTableWidgetItem* num = new QTableWidgetItem();
        QTableWidgetItem* name = new QTableWidgetItem((*var)->name());
        QTableWidgetItem* infItem = new QTableWidgetItem();
        QTableWidgetItem* depItem = new QTableWidgetItem();

        num->setData(Qt::EditRole,r+1);
        infItem->setData(Qt::EditRole,(*result)[*var].first);
        depItem->setData(Qt::EditRole,(*result)[*var].second);

        ui->indirectTable->setItem(r,0, num);
        ui->indirectTable->setItem(r,1, name);
        ui->indirectTable->setItem(r,2, infItem);
        ui->indirectTable->setItem(r,3, depItem);
        ++r;
    }
}

void AnaStructResults::on_refreshDI_clicked()
{
    displayDI();
}

void AnaStructResults::on_degSelect_valueChanged(int value)
{
    ui->degValue->setValue(value);
    displayII(value);
}

void AnaStructResults::on_degValue_valueChanged(int arg1)
{
    ui->degSelect->setValue(arg1);
    displayII(arg1);
}

void AnaStructResults::on_refreshII_clicked()
{
    displayII(ui->degValue->value());
}
