#ifndef ANASTRUCTRESULTS_H
#define ANASTRUCTRESULTS_H

#include <QWidget>
#include "src/objects/variable.h"
#include <QMap>
//#include "src/objects/matrixdirectinfluences.h"
#include <QTableWidget>
#include "src/objects/project.h"
#include <QPair>

namespace Ui {
class AnaStructResults;
}

class AnaStructResults : public QWidget
{
    Q_OBJECT

public:
    explicit AnaStructResults(Project* project, QWidget *parent = nullptr);
    ~AnaStructResults();

private slots:
    void on_refreshDI_clicked();

    void on_degSelect_valueChanged(int value);

    void on_degValue_valueChanged(int arg1);

    void on_refreshII_clicked();

private:
    Ui::AnaStructResults *ui;
    Project* m_project;
    int m_defaultDegII;
    void displayDI();
    void displayII(int const& deg);
};

#endif // ANASTRUCTRESULTS_H
