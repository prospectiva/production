#ifndef LICENSE_H
#define LICENSE_H

#include <QDialog>
#include <QUrl>
#include <QDesktopServices>
#include <QTextBrowser>

namespace Ui {
class License;
}

class License : public QDialog
{
    Q_OBJECT

public:
    explicit License(QWidget *parent = nullptr);
    ~License();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::License *ui;
};

#endif // LICENSE_H
