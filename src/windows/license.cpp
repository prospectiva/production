#include "license.h"
#include "ui_license.h"

License::License(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::License)
{
    ui->setupUi(this);
    ui->version->setText("V0.2");
    ui->license->setText("ProspectiVA is distributed under GNU GPLv3 license.");
    ui->licence->setText("ProspectiVA est distribué sous la license GNU GPLv3.");

//    ui->licenceText->setSource(QUrl("../../README.md"));
//    ui->licenceText->setSource(QUrl("createproject.cpp"));
}

License::~License()
{
    delete ui;
}

void License::on_pushButton_2_clicked()
{
    //TODO: set urls in variables
    QDesktopServices::openUrl (QUrl("https://gitlab.com/prospectiva/production"));
}

void License::on_pushButton_clicked()
{
    //TODO: set urls in variables
    QDesktopServices::openUrl (QUrl("https://prospectiva.gitlab.io/"));
}
