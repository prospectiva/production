#ifndef OPENEDPROJECTINFOS_H
#define OPENEDPROJECTINFOS_H

#include <QWidget>
#include "src/objects/project.h"

namespace Ui {
class OpenedProjectInfos;
}

class OpenedProjectInfos : public QWidget
{
    Q_OBJECT

public:
    explicit OpenedProjectInfos(Project* project, QWidget *parent = nullptr);
    ~OpenedProjectInfos();

private:
    Ui::OpenedProjectInfos *ui;
    Project* m_project;
};

#endif // OPENEDPROJECTINFOS_H
