#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QCloseEvent>
#include "src/windows/createproject.h"
#include "src/objects/project.h"
#include "src/objects/tools.h"
#include "src/windows/openedprojectarea.h"
#include "src/windows/variablenewwindow.h"
#include "src/windows/variabledisplaywindow.h"
#include <QUrl>
#include <QDesktopServices>
#include "src/windows/license.h"

#include <QFile>
#include <QSettings>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    Project* currentProject();

public slots:
    void projectCreation(QString const& file);


private slots:
    void on_actionNouveauProjet_triggered();
    void on_actionOuvrirProjet_triggered();
    void on_actionAjouter_une_variable_triggered();
    void on_actionVoir_les_variables_triggered();
    void on_actionSauvegarder_triggered();

    void on_actionD_finition_des_influences_triggered();

    void on_actionR_sultats_triggered();

    void on_actionDocumentation_triggered();
    void on_actionLicence_triggered();

private:
    Ui::MainWindow *ui;
    Project* m_mainProject;
    OpenedProjectArea* m_projectArea;
    void openProject(QString const& filename);
    void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
