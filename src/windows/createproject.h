#ifndef CREATEPROJECT_H
#define CREATEPROJECT_H

#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QObject>
#include "src/objects/project.h"
#include "src/windows/mainwindow.h"

namespace Ui {
class CreateProject;
}

class CreateProject : public QDialog
{
    Q_OBJECT

public:
    explicit CreateProject(QWidget *parent = nullptr);
    ~CreateProject();

public slots:
    void on_parcourirButton_clicked();
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
signals:
    void projectCreated(QString const& file);

private:
    Ui::CreateProject *ui;
    QWidget* main;
    QString* selectedName;
    QString* selectedDirectoryPath;
};

#endif // CREATEPROJECT_H
