#include "anastructdefinitioninfluences.h"
#include "ui_anastructdefinitioninfluences.h"

AnaStructDefinitionInfluences::AnaStructDefinitionInfluences(Project* project, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AnaStructDefinitionInfluences),m_project(project)
{
    ui->setupUi(this);
    displayModel();
    ui->tableWidget->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->adParams->setVisible(false);
}

AnaStructDefinitionInfluences::~AnaStructDefinitionInfluences()
{
    delete ui;
}

void AnaStructDefinitionInfluences::displayModel(){
    QVector<Variable*> variableList = m_project->variables()->list();
    int N = variableList.size();
    ui->tableWidget->setRowCount(N);
    ui->tableWidget->setColumnCount(N);
    int r(0),c(0);

    for(QVector<Variable*>::iterator i=variableList.begin(); i!=variableList.end(); ++i)
    {
        for(QVector<Variable*>::iterator j=variableList.begin(); j!=variableList.end(); ++j)
        {
            MatrixDirectInfluences::Influence inf = m_project->anaStruct()->getMatrixDI()->get(*i,*j);
            QString value = "";
            //1 if Yes, 0 if No, "" if Unknown
            if(inf==MatrixDirectInfluences::Influence::Yes)
                value="1";
            else if(inf==MatrixDirectInfluences::Influence::No)
                value="0";
            QTableWidgetItem* infValueItem = new QTableWidgetItem(value);
            ui->tableWidget->setItem(r,c, infValueItem);
            c++;
        }
        c=0;r++;
    }

    connect(ui->tableWidget,SIGNAL(itemChanged(QTableWidgetItem *)),this,SLOT(tableWidget_itemChanged(QTableWidgetItem *)));
}
void AnaStructDefinitionInfluences::displayModelAt(const int &row, const int &col, const MatrixDirectInfluences::Influence & influence){
    QTableWidgetItem* infValueItem = new QTableWidgetItem(MatrixDirectInfluences::influenceToString(influence));
    ui->tableWidget->setItem(row,col,infValueItem);
}

void AnaStructDefinitionInfluences::dealExport(const QString &file){
    QList<QList<QString>>* table = new QList<QList<QString>>();
    QVector<Variable*> list = m_project->variables()->list();
    MatrixDirectInfluences* matrix = m_project->anaStruct()->getMatrixDI();

    QList<QString>* colsHeaderNumbers = new QList<QString>;
    QList<QString>* colsHeaderNames = new QList<QString>;

    QList<QString>* row;
    int r(1);

    for (QVector<Variable*>::iterator var1=list.begin();var1!=list.end();++var1) {
        row = new QList<QString>;
        row->push_back(QString::number(r));
        // name
        row->push_back((*var1)->name());
        //values

        for(QVector<Variable*>::iterator var2=list.begin();var2!=list.end();++var2){
            QString value = MatrixDirectInfluences::influenceToString(matrix->get(*var1,*var2));
            row->push_back(value);
        }

        //update colsNames
        colsHeaderNumbers->push_back(QString::number(r));
        colsHeaderNames->push_back((*var1)->name());

        //add row
        table->push_back(*row);
        ++r;
    }
    //add colsName in begin
        //+2 to begin at right place (1 for row number + 1 for row name)
        colsHeaderNames->push_front("Noms");colsHeaderNames->push_front("");
        colsHeaderNumbers->push_front("N°");colsHeaderNumbers->push_front("N°");
    table->push_front(*colsHeaderNames);
    table->push_front(*colsHeaderNumbers);

    //define file with .xlsx
    //TODO: Do a better way: x.xls -> x.xls.xlsx here ...
    QString myFile;
    if (!std::regex_match (Tools::qStringToString(file), std::regex("(.*).xlsx") )) {
        std::string strFile;
        strFile = Tools::qStringToString(file) + ".xlsx";
        myFile = Tools::stringToQString(strFile);
    }
    else {
        myFile = file;
    }
    CalcEdit::write(myFile,table);
}

void AnaStructDefinitionInfluences::dealImport(const QString &file){
    QList<QList<QString>>* table = CalcEdit::read(file,ui->premiReCasePourLImportLineEdit->text());
    int r(0),c(0);

    for(QList<QList<QString>>::iterator it=table->begin();it<table->end();++it){
        for(QList<QString>::iterator it2=it->begin();it2<it->end();++it2){
            MatrixDirectInfluences::Influence inf = MatrixDirectInfluences::stringToInfluence(*it2);
            changeValue(r,c,inf);
            ++c;
        }
        ++r;c=0;
    }
    displayModel();
}

void AnaStructDefinitionInfluences::changeValue(const int &row, const int &col, MatrixDirectInfluences::Influence const& influence){
    Variable* varFrom = m_project->variables()->get(row);
    Variable* varTo = m_project->variables()->get(col);

    //check if varFrom = varTo (diag)
    if(varFrom != varTo)
        m_project->anaStruct()->getMatrixDI()->set(varFrom,varTo,influence);

    // check to block inverse
    if(ui->influencesReciproques->isEnabled() && ui->influencesReciproques->checkState()==Qt::Checked){
        if(influence==MatrixDirectInfluences::Influence::Yes) {
            m_project->anaStruct()->getMatrixDI()->set(varTo,varFrom,MatrixDirectInfluences::Influence::Unknown);
            displayModelAt(col,row,MatrixDirectInfluences::Influence::Unknown);
        }
    }

    // check to complete in mirror
    if(ui->remplirAuto->isEnabled() && ui->remplirAuto->checkState()==Qt::Checked){
        if(influence==MatrixDirectInfluences::Influence::Yes) {
            m_project->anaStruct()->getMatrixDI()->set(varTo,varFrom,MatrixDirectInfluences::Influence::No);
            displayModelAt(col,row,MatrixDirectInfluences::Influence::No);
        }
    }
}

void AnaStructDefinitionInfluences::tableWidget_itemChanged(QTableWidgetItem *item)
{
    MatrixDirectInfluences::Influence influence = MatrixDirectInfluences::Influence::Unknown;
    if(item->text()=="1")
        influence = MatrixDirectInfluences::Influence::Yes;
    else if(item->text()=="0")
        influence = MatrixDirectInfluences::Influence::No;
    else if(item->text()!=""){
        QMessageBox::warning(this,"Attention","La valeur est inconnue. L'influence a donc été placée en inconnue.");
        ui->tableWidget->item(item->row(),item->column())->setText("");
    }

    changeValue(item->row(),item->column(),influence);


}

void AnaStructDefinitionInfluences::on_importButton_clicked()
{
    QString file = QFileDialog::getOpenFileName(this, "Importer une matrice en csv", QString());
    dealImport(file);
}

void AnaStructDefinitionInfluences::on_exportButton_clicked()
{
    QString file = QFileDialog::getSaveFileName(this, "Exporter la matrice en csv", QString());
    dealExport(file);
}

void AnaStructDefinitionInfluences::on_influencesReciproques_stateChanged(int arg1)
{
    if(arg1==Qt::Unchecked)
        ui->remplirAuto->setEnabled(false);
    else if(arg1==Qt::Checked)
        ui->remplirAuto->setEnabled(true);
    else // we are suppose to not have "else" case: it's ot a tristate
        ui->remplirAuto->setEnabled(true);
}

void AnaStructDefinitionInfluences::on_dispAdvParams_clicked()
{
    ui->adParams->setVisible(!ui->adParams->isVisible());
}

void AnaStructDefinitionInfluences::on_cycle_clicked()
{
    QMessageBox::information(this,"TODO","à faire");
}
