#-------------------------------------------------
#
# Project created by QtCreator 2020-05-08T02:10:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProspectiVA
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

# QXlsx code for Application Qt project
QXLSX_PARENTPATH=./src/QXlsx         # current QXlsx path is . (. means curret directory)
QXLSX_HEADERPATH=./src/QXlsx/header/  # current QXlsx header path is ./header/
QXLSX_SOURCEPATH=./src/QXlsx/source/  # current QXlsx source path is ./source/
include(./src/QXlsx/QXlsx.pri)

SOURCES += \
        main.cpp \
    src/windows/license.cpp \
    src/windows/mainwindow.cpp \
    src/objects/project.cpp \
    src/windows/createproject.cpp \
    src/objects/tools.cpp \
    src/windows/openedprojectarea.cpp \
    src/windows/openedprojectinfos.cpp \
    src/objects/variable.cpp \
    src/objects/variablelist.cpp \
    src/windows/variablenewwindow.cpp \
    src/windows/variabledisplaywindow.cpp \
    src/objects/theme.cpp \
    src/objects/themelist.cpp \
    src/windows/anastructdefinitioninfluences.cpp \
    src/objects/matrixdirectinfluences.cpp \
    src/objects/anastruct.cpp \
    src/objects/calcedit.cpp \
    src/windows/anastructresults.cpp \
    src/objects/graphgrouping.cpp

HEADERS += \
    src/windows/license.h \
    src/windows/mainwindow.h \
    src/objects/project.h \
    src/windows/createproject.h \
    src/objects/tools.h \
    src/windows/openedprojectarea.h \
    src/windows/openedprojectinfos.h \
    src/objects/variable.h \
    src/objects/variablelist.h \
    src/windows/variablenewwindow.h \
    src/windows/variabledisplaywindow.h \
    src/objects/theme.h \
    src/objects/themelist.h \
    src/windows/anastructdefinitioninfluences.h \
    src/objects/matrixdirectinfluences.h \
    src/objects/anastruct.h \
    src/objects/calcedit.h \
    src/windows/anastructresults.h \
    src/objects/graphgrouping.h

FORMS += \
    src/windows/license.ui \
    src/windows/mainwindow.ui \
    src/windows/createproject.ui \
    src/windows/openedprojectarea.ui \
    src/windows/openedprojectinfos.ui \
    src/windows/variablenewwindow.ui \
    src/windows/variabledisplaywindow.ui \
    src/windows/anastructdefinitioninfluences.ui \
    src/windows/anastructresults.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
