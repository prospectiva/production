# ProspectiVA

## How to use?

See [the ProspectiVA page](https://prospectiva.gitlab.io/)

## License and links

- This project is distributed under **GNU GPLv3** license.
- Thanks to:
  - Qt (under LGPL v3 license or Commercial license) - https://www.qt.io/
  - QXlsx (under MIT license) - https://github.com/QtExcel/QXlsx


## Contact

*ProspectiVA* was developped by [Dylan Marinho](https://dylan-marinho.gitlab.io).
